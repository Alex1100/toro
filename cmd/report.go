package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/guptarohit/asciigraph"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh/terminal"
)

type positionLister interface {
	GetPortfolioHistory(period *string, timeframe *alpaca.RangeFreq, dateEnd *time.Time, extendedHours bool) (*alpaca.PortfolioHistory, error)
	ListPositions() ([]alpaca.Position, error)
}

type reportCmd struct {
	cmd *cobra.Command

	period string
}

func newReportCmd() *reportCmd {
	r := &reportCmd{
		cmd: &cobra.Command{
			Use:   "report",
			Short: "Display a report with all open positions",
		},
	}

	r.cmd.RunE = func(cmd *cobra.Command, args []string) error {
		client, err := getAlpacaClient()

		if err != nil {
			return err
		}

		return r.GetPositions(getTabWriter(), client)
	}

	r.cmd.Flags().StringVarP(&r.period, "period", "p", "day", "time period of data. One of: day|week|month|3month|year")

	return r
}

func (r *reportCmd) GetCommand() *cobra.Command {
	return r.cmd
}

func (r *reportCmd) GetPositions(w writeFlusher, lister positionLister) error {
	positions, err := lister.ListPositions()

	if err != nil {
		return err
	}

	defer w.Flush()

	fmt.Fprintln(w, "📈 Portfolio")

	var period string
	var freq alpaca.RangeFreq
	var cap string

	switch r.period {
	case PeriodDay:
		period = "1D"
		freq = alpaca.Min15
		cap = "1 Day"
	case PeriodWeek:
		freq = alpaca.Hour1
		period = "1W"
		cap = "1 Week"
	case Period1Month:
		freq = alpaca.Day1
		period = "1M"
		cap = "1 Month"
	case Period3Month:
		freq = alpaca.Day1
		period = "3M"
		cap = "3 Months"
	case Period1Year:
		freq = alpaca.Day1
		period = "1A"
		cap = "1 Year"
	}

	history, err := lister.GetPortfolioHistory(&period, &freq, nil, true)

	if err != nil {
		return err
	}

	historyLen := len(history.Equity)
	fmt.Fprintf(w, "  $%s\n\n", history.Equity[historyLen-1].StringFixed(2))

	data := make([]float64, historyLen)

	for i := 0; i < historyLen; i++ {
		// fmt.Printf("%d %v %v %v\n", i, history.Timestamp[i], history.Equity[i], history.ProfitLoss[i])
		price := history.Equity[i]
		data[i], _ = price.Float64()
	}

	opts := []asciigraph.Option{asciigraph.Caption(cap)}

	width, _, _ := terminal.GetSize(int(os.Stdout.Fd()))

	if historyLen > width {
		opts = append(opts, asciigraph.Width(width-10))
	}

	opts = append(opts, asciigraph.Height(10))

	chart := asciigraph.Plot(data, opts...)
	fmt.Fprintln(w, chart)

	fmt.Fprintln(w, "")

	fmt.Fprintln(w, "💼 Stocks")
	fmt.Fprintln(w, "Stock\tShares\tPrice\tAvg Cost\tEquity\tToday's Return (%)\tTotal Return (%)\t")

	for _, pos := range positions {
		pctReturnToday := pos.ChangeToday.Shift(2)
		pctReturnTotal := pos.CurrentPrice.Sub(pos.EntryPrice).Div(pos.EntryPrice).Shift(2)

		fmt.Fprintf(
			w,
			"%s\t%s\t%s\t%s\t%s\t%s\t%s\t\n",
			pos.Symbol,
			pos.Qty.String(),
			pos.CurrentPrice.StringFixed(2),
			pos.EntryPrice.StringFixed(2),
			pos.MarketValue.StringFixed(2),
			pctReturnToday.StringFixed(2),
			pctReturnTotal.StringFixed(2),
		)
	}

	return nil
}
