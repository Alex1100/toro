package cmd

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
)

type configCmd struct {
	cmd *cobra.Command
}

func newConfigCmd() *configCmd {
	c := &configCmd{
		cmd: &cobra.Command{
			Use:   "config",
			Short: "Adjust config values",
			Long:  `Adjust config values, including account credentials and other options`,
		},
	}

	c.cmd.AddCommand(newSetCredentialsCmd().GetCommand())
	c.cmd.AddCommand(newSetModeCmd().GetCommand())

	return c
}

func (c *configCmd) GetCommand() *cobra.Command {
	return c.cmd
}

type setCredentialsCmd struct {
	cmd *cobra.Command

	id     string
	secret string
}

func newSetCredentialsCmd() *setCredentialsCmd {
	setCreds := &setCredentialsCmd{
		cmd: &cobra.Command{
			Use:   "set-credentials",
			Short: "Provide credentials for authenticating your Alpaca account",
			Long:  `Provide credentials for authenticating your Alpaca account`,
		},
	}

	setCreds.cmd.RunE = func(cmd *cobra.Command, args []string) error {
		if setCreds.id == "" && setCreds.secret == "" {
			return errors.New("No credentials provided")
		}

		if setCreds.id != "" {
			err := writeConfig(cfgFile, "id", setCreds.id)

			if err == nil {
				fmt.Printf("✅ Set id successfully\n")
			} else {
				return fmt.Errorf("Failed to save id: %w", err)
			}
		}

		if setCreds.secret != "" {
			err := writeConfig(cfgFile, "secret", setCreds.secret)

			if err == nil {
				fmt.Printf("✅ Set secret successfully\n")
			} else {
				return fmt.Errorf("Failed to save secret: %w", err)
			}
		}

		return nil
	}

	setCreds.cmd.Flags().StringVar(&setCreds.id, "id", "", "id for api access")
	setCreds.cmd.Flags().StringVar(&setCreds.secret, "secret", "", "secret for api access")

	return setCreds
}

func (setCreds *setCredentialsCmd) GetCommand() *cobra.Command {
	return setCreds.cmd
}

type setModeCmd struct {
	cmd *cobra.Command
}

func newSetModeCmd() *setModeCmd {
	return &setModeCmd{
		cmd: &cobra.Command{
			Use:   "set-mode (live|paper)",
			Short: "Select whether trade will be made using your \"live\" or \"paper\" account",
			RunE: func(cmd *cobra.Command, args []string) error {
				if len(args) < 1 {
					return errors.New("Mode not provided")
				}

				mode := args[0]

				if !(mode == "paper" || mode == "live") {
					return fmt.Errorf("Mode \"%s\" invalid. Mode must be \"live\" or \"paper\"", mode)
				}

				err := writeConfig(cfgFile, "mode", mode)

				if err != nil {
					return err
				}

				fmt.Printf("✅ Mode set to \"%s\"\n", mode)

				return nil
			},
		},
	}
}

func (setMode *setModeCmd) GetCommand() *cobra.Command {
	return setMode.cmd
}

func writeConfig(file string, key string, value string) error {
	_, err := os.Stat(file)

	var fileContents []byte
	if os.IsNotExist(err) {
		fileContents = []byte("")
	} else if err != nil {
		return fmt.Errorf("Error opening file %s: %w", file, err)
	} else {
		fileContents, err = ioutil.ReadFile(file)

		if err != nil {
			return fmt.Errorf("Error reading file %s: %w", file, err)
		}
	}

	data := map[string]string{}

	yaml.Unmarshal(fileContents, data)

	data[key] = value

	configBytes, err := yaml.Marshal(data)

	if err != nil {
		return fmt.Errorf("Error marshalling config to yaml: %w", err)
	}

	err = ioutil.WriteFile(cfgFile, configBytes, os.ModePerm)

	if err != nil {
		return fmt.Errorf("Error persisting config: %w", err)
	}

	return err
}
