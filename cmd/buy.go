package cmd

import (
	"fmt"
	"strings"

	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/spf13/cobra"
	"gitlab.com/aenegri/toro/order"
)

type orderer interface {
	PlaceOrder(req alpaca.PlaceOrderRequest) (*alpaca.Order, error)
}

// buyCmd is used to purchase stocks
type buyCmd struct {
	cmd          *cobra.Command
	supportedTIF map[string]bool

	// flags
	qty         int
	limitPrice  float64
	stopPrice   float64
	timeInForce string
}

// newBuyCmd creates a new instance of buyCmd
func newBuyCmd() *buyCmd {
	buy := &buyCmd{
		cmd: &cobra.Command{
			Use:   "buy SYMBOL",
			Short: "Place an order to buy stocks",
			Long:  `Place an order to buy stocks`,
			Args:  cobra.ExactArgs(1),
		},
		supportedTIF: map[string]bool{
			"day": true,
			"gtc": true,
			"opg": true,
			"cls": true,
			"ioc": true,
			"fok": true,
		},
	}

	buy.cmd.RunE = func(cmd *cobra.Command, args []string) error {
		client, err := getAlpacaClient()

		if err != nil {
			return err
		}

		return buy.Buy(strings.ToUpper(args[0]), client)
	}

	buy.cmd.Flags().IntVarP(&buy.qty, "quantity", "q", 1, "number of stocks to purchase")
	buy.cmd.Flags().Float64VarP(&buy.limitPrice, "limit-price", "l", 0, "the maximum price to pay for a share")
	buy.cmd.Flags().Float64VarP(&buy.stopPrice, "stop-price", "s", 0, "price trigger to start order")
	buy.cmd.Flags().StringVar(
		&buy.timeInForce,
		"time-in-force",
		"day",
		"indicates how long an order will remain active before it is executed or expires. One of: day|gtc|opg|cls|ioc|fok. See alpaca documentation for details [https://alpaca.markets/docs/trading-on-alpaca/orders/#time-in-force]",
	)

	return buy
}

// GetCommand retrieves the embedded *cobra.Command
func (b *buyCmd) GetCommand() *cobra.Command {
	return b.cmd
}

// Buy uses the buyer to place a buy order.
func (b *buyCmd) Buy(symbol string, buyer orderer) error {
	req, err := order.NewBuilder().
		Side(alpaca.Buy).
		Symbol(symbol).
		Quantity(b.qty).
		LimitPrice(b.limitPrice).
		StopPrice(b.stopPrice).
		Build()

	if err != nil {
		return err
	}

	_, err = buyer.PlaceOrder(req)

	if err != nil {
		return err
	}

	fmt.Println("✅ Order placed")

	return nil
}
