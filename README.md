# toro

Trade stocks on the command line using your Alpaca account

![cli example](https://toro.nyc3.cdn.digitaloceanspaces.com/toro-report.png)

## Usage

- `toro report`
- `toro get SYMBOL`
- `toro buy SYMBOL`
- `toro sell SYMBOL`
- `toro orders`
- `toro cancel ORDER_ID`
- `toro config [set-credentials set-mode]`

## Setup

### Install toro

```bash
git clone https://gitlab.com/aenegri/toro.git
cd toro
go install
```

### Signup for Alpaca

Create your brokerage account with [alpaca](https://app.alpaca.markets/signup)

### Configure toro

First, provide credentials for toro to access your Alpaca account

```bash
toro config set-credentials --id={id} --secret={secret}
```

Then, decide whether to trade with your paper or live account

```bash
toro config set-mode {paper|live}
```